
# Python script to get all the list of files and
# prepare the copy script

import os
import sys, getopt

def get_directory_list(dir_path):
    direcotry_name = [dir_path]
    for r,d,f in os.walk(dir_path):
        d[:] = [dir for dir in d if not dir.startswith('.')]
        for dirs in d:
            direcotry_name.append(r + "/" + dirs)
            
    return direcotry_name

def get_target_directory_list(dir_path, dir_list):
    target_dir_list = []

    for name in dir_list:
        name = name.replace("'", " ")
        name = name.strip()
        name_list = name.split("/")
        i = 0;
        while i < len(name_list):
            name_list[i] = name_list[i].strip()
            name_list[i] = name_list[i].replace(" ", "_")
            i += 1
        target_name = "/".join(name_list)
        target_dir_list.append(dir_path + "/" + target_name)
    return target_dir_list

def get_file_list(dir_path):
    file_list = []
    f_list = os.listdir(dir_path)
    for f in f_list:
        if (f.startswith('.') == False):
            f = dir_path + "/" + f
            if (os.path.isfile(f)):
                file_list.append(f)
    return file_list

def get_target_file_list(target_dir_path, f_list):
    file_list = []
    for name in f_list:
        name = name.replace("'", " ")
        name = name.strip()
        name = name.replace(" ", "_")
        file_list.append(target_dir_path + name)
    return file_list

def write_directory_list(file, dir_list):
    file.write('echo "Directory list creation start"')
    file.write("\n")
    for d in dir_list:
        file.write("mkdir -p " + d + "\n")
    file.write('echo "Directory list creation complete"')
    file.write("\n")

def write_file_list(file, file_list, target_file_list):
    index = 0
    while(index < len(file_list)):
        file.write("pv " + file_list[index] + " > " + target_file_list[index] + "\n")
        index += 1
    
    

src_dir_path = os.environ.get('SRC_DIR_PATH', os.getcwd())
dest_dir_path = os.environ.get('DEST_DIR_PATH', " ")
exec_file_path = os.environ.get('EXEC_FILE_PATH', " ")

if (dest_dir_path == " "):
    print("Destination directory is not set... Exit ...")
    exit()
if (exec_file_path == " "):
    print("Execution is not set... Exit ...")
    exit()

src_dir_path = src_dir_path.strip()
dest_dir_path = dest_dir_path.strip()
exec_file_path = exec_file_path.strip()
if (dest_dir_path[-1] == '/'):
    dest_dir_path = dest_dir_path[:-1]
    dest_dir_path = dest_dir_path.strip()
file = open(exec_file_path, 'w')

dir_name_list = get_directory_list(src_dir_path)
target_dir_name_list = get_target_directory_list(dest_dir_path, dir_name_list)
write_directory_list(file,target_dir_name_list)

if (len(dir_name_list) != len(target_dir_name_list)):
    print ("Both src dir list and target dir list is not same " + str(len(dir_name_list)) + "!=" + str(len(target_dir_name_list)))
    exit()

i = 0
for dir_name in dir_name_list:
    target_dir_name = target_dir_name_list[i]
    i += 1
    desc = "Directory index " + str(i) + " in max " + str(len(dir_name_list))
    file.write('echo "' + desc + '"\n')
    src_list = get_file_list(dir_name)
    target_list = get_target_file_list(target_dir_name, src_list)
    write_file_list(file, src_list, target_list)

file.close()






